package me.projectx.itemshowcase.managers;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import me.projectx.Economy.Managers.AccountManager;
import me.projectx.Economy.Utils.DatabaseUtils;
import me.projectx.itemshowcase.ItemShowcase;
import me.projectx.itemshowcase.enums.Message;
import me.projectx.itemshowcase.models.Shop;
import me.projectx.itemshowcase.utils.ItemUtils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class ShopManager {

	private static ShopManager sm = new ShopManager();
	public List<Shop> shops = new ArrayList<Shop>();
	public List<UUID> delete = new ArrayList<UUID>();

	public static ShopManager getManager() {
		return sm;
	}

	public Shop getShop(UUID id) {
		for (Shop s : shops) {
			if (s.getOwner().equals(id)) {
				return s;
			}
		}
		return null;
	}

	public Shop getShop(Location location) {
		for (Shop s : shops) {
			if (s.getLocation().equals(location)) {
				return s;
			}
		}
		return null;
	}

	public boolean isShop(Block block) {
		Location l = block.getLocation();
		for (Shop s : shops) {
			if (s.getLocation().equals(l)) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isShop(Item item){
		for(Shop s : this.shops){
			if(s.getItemUUID().equals(item.getUniqueId())){
				return true;
			}
		}
		return false;
	}

	public void createShop(Player player, ItemStack item, Location location, double price) throws SQLException {
		Shop s = new Shop(player.getUniqueId(), location, player.getItemInHand().clone(), price);
		shops.add(s);
		Item i = s.loadItem();
		DatabaseUtils
		.queryOut("INSERT INTO shops(id, location, owner, uuid, item, price) VALUES ("
				+ s.getId()
				+ ", '"
				+ locationToString(location)
				+ "', '"
				+ player.getUniqueId().toString()
				+ "', '"
				+ i.getUniqueId().toString()
				+ "', '"
				+ ItemUtils.seralizeItem(item) 
				+ "', '" 
				+ price 
				+ "');");
	}
	
	public void loadShop(String location, UUID owner, UUID item, ItemStack itemstack, double price, int id){
		Shop s = new Shop(location, owner, item, itemstack, price);
		s.setId(id);
		shops.add(s);
	}

	public void deleteShop(Player p, Block block) {
		if (delete.contains(p.getUniqueId())){
			Shop s = getShop(block.getLocation());
			if (s != null) {
				removeItem(s);
				shops.remove(s);
				delete.remove(p.getUniqueId());
				try {
					DatabaseUtils.queryOut("DELETE FROM shops WHERE location='" + locationToString(block.getLocation()) + "';");
				} catch(SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void removeItem(Shop s) { //Optimize so it doesn't have to loop through all entities?
		UUID id = s.getItemUUID();
		for (Entity e : s.getLocation().getWorld().getEntities()) {
			if (e instanceof Item) {
				if (e.getUniqueId().equals(id)){
					e.remove();
				}
			}
		}
	}

	public void updateShops(long delay, long seconds) {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(ItemShowcase.getInstance(), new Runnable() {
			@Override
			public void run() {
				for (Shop s : shops) {
					if(s.isLoaded()){
						Entity e = s.getEntity();
						e.teleport(s.getLocation());
					}
				}
			}
		}, delay, seconds * 20);
	}

	@SuppressWarnings("deprecation")
	public void initiatePurchase(PlayerInteractEvent e) {
		if (e.getClickedBlock() != null){
			Shop s = getShop(e.getClickedBlock().getLocation());
			if (s != null) {
				if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					Player p = e.getPlayer();
					if (AccountManager.getManager().getAccount(p).getBalance() >= s.getPrice()){
						int amount = 1;
						if (p.isSneaking()){
							amount = 64;
						}
						for (int i = 0; i < amount; i++)
							p.getInventory().addItem(s.getItem().clone());
							p.updateInventory(); //without this the item doesn't seem display in the inventory due to graphical bug

							AccountManager.getManager().withdraw(e.getPlayer(), s.getPrice() * amount);
							p.sendMessage(Message.PURCHASE.getMsg().replace("<item>", s.getItem().getType().toString()).replace("<price>", 
									Double.valueOf((s.getPrice() * amount)).toString()));
					}else{
						p.sendMessage(Message.INSUFFICIENT_FUNDS.getMsg());
					}
				}
			}
		}
	}

	public void displayShopInfo(PlayerInteractEvent e) {
		if (e.getAction() == Action.LEFT_CLICK_BLOCK && e.getClickedBlock() != null) {
			Shop s = getShop(e.getClickedBlock().getLocation());
			if (s != null) {
				Player p = e.getPlayer();
				p.sendMessage(ChatColor.GRAY + "Shop ID: " + ChatColor.AQUA
						+ s.getId());
				p.sendMessage(ChatColor.GRAY + "Owner: "
						+ Bukkit.getPlayer(s.getOwner()).getDisplayName());
				p.sendMessage(ChatColor.GRAY + "Price: " + ChatColor.AQUA
						+ s.getPrice());
				p.sendMessage(ChatColor.GRAY + "Item: " + ChatColor.AQUA
						+ s.getItem().getType().name());
			}
		}
	}
	
	public void loadChunk(Chunk c){
		for(Shop s : this.shops){
			if(s.getLocation().getChunk()==c){
				s.setLoaded(true);
			}
		}
	}
	
	public void unloadChunk(Chunk c){
		for(Shop s : this.shops){
			if(s.getLocation().getChunk()==c){
				s.setLoaded(false);
			}
		}
	}
	
	public Location getLocation(String location){
		if(location!=null){
			String[] args = location.split(", ");
			if(args.length>3){
				if(args.length>5){
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]),
							Float.parseFloat(args[4]), Float.parseFloat(args[5]));
				}else{
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
				}
			}
		}
		return null;
	}

	public String locationToString(Location location){
		if(location!=null){
			return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", " + location.getYaw() + ", " + location.getPitch();
		}
		return null;
	}
}
