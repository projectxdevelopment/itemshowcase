package me.projectx.itemshowcase.managers;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class CreateManager {

	public Map<UUID, Double> players = new HashMap<UUID, Double>();
	public Map<UUID, ItemStack> item = new HashMap<UUID, ItemStack>();
	private static CreateManager cm = new CreateManager();

	public static CreateManager getManager(){
		return cm;
	}

	public void placeItem(PlayerInteractEvent e){
		if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
			if (players.containsKey(e.getPlayer().getUniqueId())) {
				e.setCancelled(true);
				try {
					ShopManager.getManager().createShop(e.getPlayer(), item.get(e.getPlayer().getUniqueId()), e.getClickedBlock().getLocation(), players.get(e.getPlayer().getUniqueId()));
				} catch(SQLException e1) {
					e1.printStackTrace();
				}
				removePlayer(e.getPlayer());
				e.getPlayer().sendMessage(ChatColor.GREEN + "Shop created!");
			}
		}
	}

	public void addPlayer(Player player, double d, ItemStack i) {
		player.sendMessage(ChatColor.GOLD + "Creating a shop! Left click where you want to make one!");
		players.put(player.getUniqueId(), d);
		item.put(player.getUniqueId(), i);
	}

	public void removePlayer(Player player) {
		players.remove(player.getUniqueId());
		item.remove(player.getUniqueId());
	}
}
