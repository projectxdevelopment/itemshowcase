package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockPlace implements Listener {

	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		if (ShopManager.getManager().isShop(e.getBlock().getLocation().add(0, -1, 0).getBlock())){
			if (e.getItemInHand().getType() != Material.AIR){
				e.setCancelled(true);
			}
		}
	}
}
