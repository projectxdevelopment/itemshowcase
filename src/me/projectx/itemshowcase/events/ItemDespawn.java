package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.ItemDespawnEvent;

public class ItemDespawn implements Listener {

	@EventHandler
	public void onPlace(ItemDespawnEvent e){
		if(!e.isCancelled())e.setCancelled(ShopManager.getManager().isShop(e.getEntity()));
	}

}
