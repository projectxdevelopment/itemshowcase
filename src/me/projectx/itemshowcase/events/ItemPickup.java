package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class ItemPickup implements Listener {
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent e){
		if(!e.isCancelled())e.setCancelled(ShopManager.getManager().isShop(e.getItem().getLocation().getBlock()));
	}
}
