package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.CreateManager;
import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteract implements Listener {

	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		ShopManager.getManager().displayShopInfo(e);
		ShopManager.getManager().initiatePurchase(e);
		CreateManager.getManager().placeItem(e);
		if (e.getAction() == Action.LEFT_CLICK_BLOCK){
			ShopManager.getManager().deleteShop(e.getPlayer(), e.getClickedBlock());
		}
	}
}
