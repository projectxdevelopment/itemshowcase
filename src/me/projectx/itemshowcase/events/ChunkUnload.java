package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class ChunkUnload implements Listener {

	@EventHandler
	public void onPlace(ChunkUnloadEvent e){
		ShopManager.getManager().unloadChunk(e.getChunk());
	}

}
