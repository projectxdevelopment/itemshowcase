package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;

public class ChunkLoad implements Listener {

	@EventHandler
	public void onPlace(ChunkLoadEvent e){
		ShopManager.getManager().loadChunk(e.getChunk());
	}

}
