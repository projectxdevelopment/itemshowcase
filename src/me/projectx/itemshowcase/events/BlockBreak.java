package me.projectx.itemshowcase.events;

import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockBreak implements Listener {
	
	@EventHandler
	public void onBreak(BlockBreakEvent e){
		if (ShopManager.getManager().isShop(e.getBlock())){
			e.setCancelled(true);
		}
	}
}
