package me.projectx.itemshowcase.enums;

import org.bukkit.ChatColor;

public enum Message {
	
	INSUFFICIENT_FUNDS("&4Insufficient funds to purchase that item!"),
	PURCHASE("&7Purchased &b<item> &7for &b$<price>"),
	SHOP_NOT_OWNER("&4You must be the owner of a shop to delete it!"),
	SHOP_CREATE_CANCEL("&4Shop creation cancelled."),
	INVALID_ARGS("&4Invalid arguments. Try "),
	NO_PERMS("&4You don't have permission to do that!");
	
	private String msg;
	
	Message(String msg){
		this.msg = msg;
	}
	
	public String getMsg(){
		return ChatColor.translateAlternateColorCodes('&', msg);
	}
}
