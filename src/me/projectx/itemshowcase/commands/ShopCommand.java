package me.projectx.itemshowcase.commands;

import me.projectx.itemshowcase.enums.Message;
import me.projectx.itemshowcase.managers.CreateManager;
import me.projectx.itemshowcase.managers.ShopManager;
import me.projectx.itemshowcase.models.CommandModel;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ShopCommand extends CommandModel{

	public ShopCommand() {
		super("isc.shop", "/isc <create> || <cancel> || <delete>");
	}

	@Override
	public boolean onCmd(CommandSender sender, String str, String[] args) {
		if (args.length > 0){
			Player p = (Player)sender;
			switch(args[0]){
				case "create":
					if (args.length == 2){
						if (p.hasPermission("isc.shop.create")){
							CreateManager.getManager().addPlayer(p, new Double(args[1]), p.getItemInHand());
							return true;
						}else{
							p.sendMessage(Message.NO_PERMS.getMsg());
							return true;
						}
					}else{
						p.sendMessage(Message.INVALID_ARGS.getMsg() + "/isc create <price>");
						return true;
					}
				case "cancel":
					if (args.length == 1){
						if (p.hasPermission("isc.shop.create.cancel")){
							p.sendMessage(Message.SHOP_CREATE_CANCEL.getMsg());
							CreateManager.getManager().removePlayer(p);
							return true;
						}else{
							p.sendMessage(Message.NO_PERMS.getMsg());
							return true;
						}	
					}else{
						p.sendMessage(Message.INVALID_ARGS.getMsg() + "Try /isc cancel");
						return true;
					}
				case "delete":
					if (args.length == 1){
						if (p.hasPermission("isc.shop.delete")){
							ShopManager.getManager().delete.add(p.getUniqueId());
							p.sendMessage(ChatColor.GOLD + "Click on the shop to delete!");
							return true;
						}else{
							p.sendMessage(Message.NO_PERMS.getMsg());
							return true;
						}
					}else{
						p.sendMessage(Message.INVALID_ARGS.getMsg() + "Try /isc delete");
						return true;
					}
				default:
					p.sendMessage(ChatColor.DARK_RED + "Invalid command argument specified.");
					break;
			}
		}else{
			return false;
		}
		return false;
	}
}
