package me.projectx.itemshowcase;

import me.projectx.itemshowcase.managers.ShopManager;
import me.projectx.itemshowcase.models.Shop;
import me.projectx.itemshowcase.utils.DatabaseUtils;
import me.projectx.itemshowcase.utils.Startup;

import org.bukkit.plugin.java.JavaPlugin;

public class ItemShowcase extends JavaPlugin {

	private static ItemShowcase plugin;

	public void onEnable() {
		plugin = this;
		Startup.runStartup();
	}

	public void onDisable() {
		for (Shop s : ShopManager.getManager().shops){
			ShopManager.getManager().removeItem(s);
		}
		DatabaseUtils.closeConnection();
		plugin = null;
	} 

	public static ItemShowcase getInstance() {
		return plugin;
	}
}
