package me.projectx.itemshowcase.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import me.projectx.itemshowcase.ItemShowcase;
import me.projectx.itemshowcase.commands.ShopCommand;
import me.projectx.itemshowcase.events.*;
import me.projectx.itemshowcase.managers.ShopManager;

import org.bukkit.Bukkit;

public class Startup {

	public static void runStartup(){
		ItemShowcase.getInstance().saveDefaultConfig();
		DatabaseUtils.setupConnection();
		try {
			DatabaseUtils.setupMySQL();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			ResultSet res = DatabaseUtils.queryIn("SELECT * FROM shops;");
			while (res.next()) {
				UUID owner = UUID.fromString(res.getString("owner"));
				UUID item = UUID.fromString(res.getString("uuid"));
				String location = res.getString("location");
				int id = res.getInt("id");
				ShopManager.getManager().loadShop(location, owner, item, ItemUtils.unseralizeItem(res.getString("item")), res.getDouble("price"), id);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		ItemShowcase.getInstance().getCommand("isc").setExecutor(new ShopCommand());
		Bukkit.getPluginManager().registerEvents(new BlockBreak(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new BlockPlace(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new ChunkLoad(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new ChunkUnload(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new ItemDespawn(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new ItemPickup(), ItemShowcase.getInstance());
		Bukkit.getPluginManager().registerEvents(new PlayerInteract(), ItemShowcase.getInstance());


		ShopManager.getManager().updateShops(0, 10);
	}

}
