package me.projectx.itemshowcase.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.MaterialData;

public class ItemUtils {

	public static String seralizeItem(ItemStack i) {
		String item = i.getType().name() + "," + i.getAmount() + ","
				+ i.getDurability();
		return item;
	}

	@SuppressWarnings("deprecation")
	public static ItemStack unseralizeItem(String s) {
		String[] args = s.split(",");
		ItemStack item = new MaterialData(Material.valueOf(args[0]),
				Byte.valueOf(args[1])).toItemStack();
		item.setAmount(Integer.valueOf(args[1]));
		return item;
	}
}
