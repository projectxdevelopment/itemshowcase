package me.projectx.itemshowcase.models;

import java.util.Random;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;

public class Shop {

	private String ownerID, itemID;
	private int id;
	private double price;
	private String location;
	private ItemStack item;
	private boolean loaded = false;

	public Shop(UUID owner, Location location, ItemStack itemstack, double price) {
		this.ownerID = owner.toString();
		this.location = locationToString(location);
		this.item = itemstack.clone();
		this.price = price;
		this.id = new Random().nextInt();
	}
	
	public Shop(String location, UUID owner, UUID item, ItemStack itemstack, double price){
		this.ownerID = owner.toString();
		this.location = location;
		this.itemID = item.toString();
		this.item = itemstack;
		this.price = price;
		this.id = new Random().nextInt();
	}

	public UUID getOwner() {
		return UUID.fromString(ownerID);
	}
	
	public UUID getItemUUID(){
		return UUID.fromString(itemID);
	}

	public void setOwner(UUID id) {
		this.ownerID = id.toString();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Location getLocation() {
		return getLocation(location);
	}

	public ItemStack getItem() {
		return item;
	}

	public void setItem(ItemStack itemstack){
		this.item = itemstack;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}
	
	public Entity getEntity(){
		for(Entity e : getLocation().getWorld().getEntities()){
			if(e.getUniqueId().toString().equals(this.itemID)){
				return e;
			}
		}
		return null;
	}

	public Item loadItem(){
		Location l = getLocation();
		ItemStack is = item;
		ItemMeta im = is.getItemMeta();
		im.setDisplayName("�rStore Item");
		is.setItemMeta(im);
		Item i = l.getWorld().dropItem(l.add(.5,1.5,.5), is);
		i.setVelocity(new Vector(0,0,0));
		this.itemID = i.getUniqueId().toString();
		this.loaded = true;
		return i;
	}

	public Location getLocation(String location){
		if(location!=null){
			String[] args = location.split(", ");
			if(args.length>3){
				if(args.length>5){
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]),
							Float.parseFloat(args[4]), Float.parseFloat(args[5]));
				}else{
					return new Location(Bukkit.getWorld(args[0]), Double.parseDouble(args[1]), Double.parseDouble(args[2]), Double.parseDouble(args[3]));
				}
			}
		}
		return null;
	}

	public String locationToString(Location location){
		if(location!=null){
			return location.getWorld().getName() + ", " + location.getX() + ", " + location.getY() + ", " + location.getZ() + ", " + location.getYaw() + ", " + location.getPitch();
		}
		return null;
	}
	
	public boolean isLoaded(){
		return loaded;
	}
	
	public void setLoaded(boolean value){
		this.loaded = value;
	}
}
